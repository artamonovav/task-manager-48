package ru.t1.artamonov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.artamonov.tm.api.service.IConnectionService;
import ru.t1.artamonov.tm.api.service.dto.IProjectDTOService;
import ru.t1.artamonov.tm.api.service.dto.IUserDTOService;
import ru.t1.artamonov.tm.dto.model.ProjectDTO;
import ru.t1.artamonov.tm.dto.model.UserDTO;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.marker.UnitCategory;
import ru.t1.artamonov.tm.service.dto.ProjectDTOService;
import ru.t1.artamonov.tm.service.dto.UserDTOService;

import java.util.List;
import java.util.UUID;

import static ru.t1.artamonov.tm.constant.ProjectTestData.*;
import static ru.t1.artamonov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @Nullable
    private static IUserDTOService userService;

    @Nullable
    private static IProjectDTOService projectService;

    @BeforeClass
    public static void init() {
        @NotNull PropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        userService = new UserDTOService(connectionService, propertyService);
        projectService = new ProjectDTOService(connectionService);
        if (userService.findOneById(USER1.getId()) == null) userService.add(USER1);
        if (userService.findOneById(USER2.getId()) == null) userService.add(USER2);
        if (userService.findOneById(ADMIN.getId()) == null) userService.add(ADMIN);
    }

    @AfterClass
    public static void clear() {
        @Nullable List<UserDTO> users = userService.findAll();
        if (users != null && users.size() > 0)
            for (@NotNull UserDTO user : users) userService.remove(user);
    }

    @Test
    public void add() {
        projectService.add(USER1_PROJECT1);
        Assert.assertTrue(projectService.existsById(USER1_PROJECT1.getId()));
        projectService.removeById(USER1_PROJECT1.getId());
    }

    @Test
    public void addByUserId() {
        @NotNull ProjectDTO project = projectService.create(USER1.getId(), UUID.randomUUID().toString());
        Assert.assertTrue(projectService.existsById(project.getId()));
        Assert.assertEquals(USER1.getId(), projectService.findOneById(project.getId()).getUserId());
        projectService.removeById(project.getId());
    }

    @Test
    public void clearByUserId() {
        projectService.add(USER1_PROJECT_LIST);
        Assert.assertNotEquals(0, projectService.getSize(USER1.getId()));
        projectService.clear(USER2.getId());
        Assert.assertEquals(0, projectService.getSize(USER2.getId()));
        projectService.clear(USER1.getId());
        Assert.assertEquals(0, projectService.getSize(USER1.getId()));
        projectService.removeAll(USER1_PROJECT_LIST);
    }

    @Test
    public void findAllByUserId() {
        projectService.add(USER1_PROJECT_LIST);
        Assert.assertEquals(3, projectService.getSize(USER1.getId()));
        projectService.clear(USER1.getId());
        projectService.removeAll(USER1_PROJECT_LIST);
    }

    @Test
    public void findOneByIdByUserId() {
        projectService.add(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), projectService.findOneById(USER1.getId(), USER1_PROJECT1.getId()).getId());
        projectService.removeById(USER1_PROJECT1.getId());
    }

    @Test
    public void removeByUserId() {
        projectService.add(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), projectService.remove(USER1.getId(), USER1_PROJECT1).getId());
        Assert.assertFalse(projectService.existsById(USER1.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void removeByIdByUserId() {
        projectService.add(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1.getId(), projectService.removeById(USER1.getId(), USER1_PROJECT1.getId()).getId());
        Assert.assertFalse(projectService.existsById(USER1.getId(), USER1_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        projectService.add(USER1_PROJECT1);
        Assert.assertTrue(projectService.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(projectService.existsById(USER2_PROJECT1.getId()));
        projectService.removeById(USER1_PROJECT1.getId());
    }

    @Test
    public void changeProjectStatusById() {
        projectService.add(USER1_PROJECT1);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findOneById(USER1_PROJECT1.getId()).getStatus());
        projectService.changeProjectStatusById(USER1_PROJECT1.getUserId(), USER1_PROJECT1.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findOneById(USER1_PROJECT1.getId()).getStatus());
        projectService.removeById(USER1_PROJECT1.getId());
    }

    @Test
    public void createProjectName() {
        @NotNull ProjectDTO project = projectService.create(USER1.getId(), "test_project");
        @NotNull ProjectDTO project1 = projectService.findOneById(project.getId());
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getId(), project1.getId());
        Assert.assertEquals("test_project", project1.getName());
        Assert.assertEquals(USER1.getId(), project1.getUserId());
        projectService.removeById(project.getId());
    }

    @Test
    public void createProjectNameDescription() {
        @NotNull ProjectDTO project = projectService.create(USER1.getId(), "test_project", "test_description");
        @NotNull ProjectDTO project1 = projectService.findOneById(project.getId());
        Assert.assertNotNull(project1);
        Assert.assertEquals(project.getId(), project1.getId());
        Assert.assertEquals("test_project", project1.getName());
        Assert.assertEquals("test_description", project1.getDescription());
        Assert.assertEquals(USER1.getId(), project1.getUserId());
        projectService.removeById(project.getId());
    }

    @Test
    public void updateById() {
        @NotNull ProjectDTO project = projectService.create(USER1.getId(), "test_project", "test_description");
        projectService.updateById(USER1.getId(), project.getId(), "new name", "new description");
        @NotNull ProjectDTO project1 = projectService.findOneById(project.getId());
        Assert.assertNotNull(project1);
        Assert.assertEquals("new name", project1.getName());
        Assert.assertEquals("new description", project1.getDescription());
        projectService.removeById(project.getId());
    }

}
