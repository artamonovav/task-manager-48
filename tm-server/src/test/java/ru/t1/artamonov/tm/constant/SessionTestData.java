package ru.t1.artamonov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.dto.model.SessionDTO;
import ru.t1.artamonov.tm.enumerated.Role;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public final class SessionTestData {

    @NotNull
    public final static SessionDTO USER1_SESSION1 = new SessionDTO();

    @NotNull
    public final static SessionDTO USER1_SESSION2 = new SessionDTO();

    @NotNull
    public final static SessionDTO USER1_SESSION3 = new SessionDTO();

    @NotNull
    public final static SessionDTO USER2_SESSION1 = new SessionDTO();

    @NotNull
    public final static SessionDTO USER2_SESSION2 = new SessionDTO();

    @NotNull
    public final static SessionDTO USER2_SESSION3 = new SessionDTO();

    @NotNull
    public final static SessionDTO ADMIN1_SESSION1 = new SessionDTO();

    @NotNull
    public final static SessionDTO ADMIN1_SESSION2 = new SessionDTO();

    @NotNull
    public final static SessionDTO ADMIN1_SESSION3 = new SessionDTO();

    @NotNull
    public final static List<SessionDTO> USER1_SESSION_LIST = Arrays.asList(USER1_SESSION1, USER1_SESSION2, USER1_SESSION3);

    @NotNull
    public final static List<SessionDTO> USER2_SESSION_LIST = Arrays.asList(USER2_SESSION1, USER2_SESSION2, USER2_SESSION3);

    @NotNull
    public final static List<SessionDTO> ADMIN1_SESSION_LIST = Arrays.asList(ADMIN1_SESSION1, ADMIN1_SESSION2, ADMIN1_SESSION3);

    @NotNull
    public final static List<SessionDTO> SESSION_LIST = new ArrayList<>();

    static {
        USER1_SESSION1.setUserId(UserTestData.USER1.getId());
        USER1_SESSION1.setId(UUID.randomUUID().toString());
        USER1_SESSION1.setRole(Role.USUAL);

        USER2_SESSION1.setUserId(UserTestData.USER2.getId());
        USER2_SESSION1.setId(UUID.randomUUID().toString());
        USER2_SESSION1.setRole(Role.USUAL);

        USER1_SESSION_LIST.forEach(session -> session.setUserId(UserTestData.USER1.getId()));
        USER2_SESSION_LIST.forEach(session -> session.setUserId(UserTestData.USER2.getId()));
        ADMIN1_SESSION_LIST.forEach(session -> session.setUserId(UserTestData.ADMIN.getId()));

        USER1_SESSION_LIST.forEach(session -> session.setRole(Role.USUAL));
        USER2_SESSION_LIST.forEach(session -> session.setRole(Role.USUAL));
        ADMIN1_SESSION_LIST.forEach(session -> session.setRole(Role.ADMIN));

        SESSION_LIST.addAll(USER1_SESSION_LIST);
        SESSION_LIST.addAll(USER2_SESSION_LIST);
        SESSION_LIST.addAll(ADMIN1_SESSION_LIST);
    }

}
